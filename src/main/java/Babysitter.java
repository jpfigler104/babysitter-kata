import java.time.*;

public class Babysitter {

    private final LocalTime open = LocalTime.parse("17:00");
    private final LocalTime bedtime = LocalTime.parse("20:00");
    private final LocalTime midnight = LocalTime.parse("00:00");
    private final LocalTime endTime = LocalTime.parse("04:00");
    private boolean newDay = false;

    public int calculatePay(String arrive, String finish) {
        int totalPay = 0;
        LocalTime startTime = LocalTime.parse(arrive);
        LocalTime leaveTime = LocalTime.parse(finish);
        if(startTime.getHour() == leaveTime.getHour()) {
            return 0;
        }
        if((startTime.isAfter(midnight) || startTime.equals(midnight)) && startTime.isBefore(endTime)) {
            newDay = true;
        }
        if(startTime.getMinute() != 0) {
            if(startTime.isAfter(open) || newDay) {
                startTime = startTime.plusMinutes(60-startTime.getMinute());
            }else {
                startTime = open;
            }
        }
        if(leaveTime.getMinute() != 0) {
            if(leaveTime.isBefore(endTime) || !newDay) {
                leaveTime = leaveTime.minusMinutes(leaveTime.getMinute());
            }else {
                leaveTime = endTime;
            }
        }
        LocalTime progress = startTime;
        while(progress.isBefore(bedtime) && !newDay && !progress.equals(leaveTime)) {
            totalPay += 12;
            progress = progress.plusMinutes(60);
        }
        while(!progress.equals(midnight) && !newDay && !progress.equals(leaveTime)) {
            totalPay += 8;
            progress = progress.plusMinutes(60);
        }
        while(progress.isBefore(endTime) && !progress.equals(leaveTime)) {
            totalPay += 16;
            progress = progress.plusMinutes(60);
        }
        return totalPay;
    }
}

/*`
    5:00 PM start
    4:00 AM end
    $12/hr start-bed
    $8/hr bed-midnight
    $16/hr midnight-end
    Paid per full hour, no fractions

    Variable: bedtime hour?

    ---Feature---
    Calculate nightly charge
 */