import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

public class BabysitterTest {

    private Babysitter babysitter;

    @BeforeEach
    public void setUp() {
        babysitter = new Babysitter();
    }

    @Test
    public void paidForFirstHour() {
        assertEquals(12, babysitter.calculatePay("17:00", "18:00"));
    }

    @Test
    public void paidForFirstTwoHours() {
        assertEquals(24, babysitter.calculatePay("17:00", "19:00"));
    }

    @Test
    public void paidFromStartToFinish() {
        assertEquals(132, babysitter.calculatePay("17:00", "04:00"));
    }

    @Test
    public void paidFromStartToBedTime() {
        assertEquals(36, babysitter.calculatePay("17:00", "20:00"));
    }

    @Test
    public void paidFromBedTimeToMidnight() {
        assertEquals(32, babysitter.calculatePay("20:00", "00:00"));
    }

    @Test
    public void paidFromMidnightToEnd() {
        assertEquals(64, babysitter.calculatePay("00:00", "04:00"));
    }

    @Test
    public void paidFrom501To359() {
        assertEquals(104, babysitter.calculatePay("17:01", "03:59"));
    }

    @Test
    public void paidFrom459To401() {
        assertEquals(132, babysitter.calculatePay("16:59", "04:01"));
    }

    @Test
    public void paidFrom1201ToEnd() {
        assertEquals(48, babysitter.calculatePay("00:01", "04:00"));
    }

    @Test
    public void paidFromStartTo1159() {
        assertEquals(60, babysitter.calculatePay("17:00", "23:59"));
    }

    @Test
    public void paidFrom501To559() {
        assertEquals(0, babysitter.calculatePay("17:01", "17:59"));
    }

    @Test
    public void paidFrom559To659() {
        assertEquals(0, babysitter.calculatePay("17:59", "18:59"));
    }
}
